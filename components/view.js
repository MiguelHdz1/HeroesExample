
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Alert,
  FlatList,
  Image,
  TouchableOpacity,
  Modal
} from 'react-native';
import { Header,Card,Button } from 'react-native-elements'
//import Modal from 'react-native-modal'

export default class view extends Component {
  
  constructor(){
    super()
    this.state={
      image:'http://i.annihil.us/u/prod/marvel/i/mg/3/50/526548a343e4b.jpg',
      name:'HEROES DE MARVEL'
    }
  }
  componentDidMount(){
    const {state} = this.props.navigation;  
    this.setState({image:state.params.url_img,name:state.params.name});     
  }
  
  render() {
    const {goBack} = this.props.navigation;
    return (
    <View style={{backgroundColor:'rgb(44, 62, 80)',flex: 1 }}>
        <Header          
          centerComponent={{ text:this.state.name.toUpperCase(), style: { color: '#fff' } }}
          leftComponent={<TouchableOpacity onPress={() => goBack()}><Text style={{color:'white'}}>VOLVER</Text></TouchableOpacity>}
          statusBarProps={{backgroundColor:'rgb(44, 62, 80)',
          animated:false,
          translucent:false}}
          outerContainerStyles={styles.outerContainer}   
        />
        <Image
            source={{uri:this.state.image}}
            style={{height:'100%',width:'100%'}}
            resizeMode='contain'
        />
    </View>              
    );
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  outerContainer: {
    position: 'relative',
    backgroundColor: 'rgb(52, 73, 94)',
    borderBottomColor: 'rgb(52, 73, 94)',
    borderBottomWidth: 1,
    padding: 12,
    height: 53,
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
