
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Alert,
  FlatList,
  Image,
  TouchableOpacity,
  Modal
} from 'react-native';
import { Header,Card,Button } from 'react-native-elements';
import { NavigationActions } from 'react-navigation';

export default class Home extends Component {
  
  constructor(){
    super()
    this.state={
      data:[],
      copyright:'',
      isModalVisible:false
    }
  }
  componentDidMount(){
    fetch('https://gateway.marvel.com/v1/public/characters?limit=100&offset=200&ts=1&apikey=b07c00b4c0462f0042653f116db2f582&hash=dd0c8c2922eae890c24427a02a2d9b6c')
    .then((response) => response.json())
    .then((responseJson) => {
      this.setState({data:responseJson.data.results,copyright:responseJson.copyright});            
    })
    .catch((error) => {
      console.error(error);
    });        
  }
  
  render() {
    const {navigate} = this.props.navigation;
    return (
      <View style={{backgroundColor:'rgb(44, 62, 80)',paddingBottom: 64}} >
        <Header          
          centerComponent={{ text: 'HEROES DE MARVEL', style: { color: '#fff' } }}
          statusBarProps={{backgroundColor:'rgb(44, 62, 80)',
          animated:false,
          translucent:false}}
          outerContainerStyles={styles.outerContainer}   
        />
          <FlatList
          data={this.state.data}
          keyExtractor={(x,i)=>i}
          renderItem={
            ({item})=>
            <Card
              containerStyle={{backgroundColor:'#1C1C1C'}}
              featuredTitle={item.name}
              titleStyle={{color:'white'}}
              image={{ uri:item.thumbnail.path+'.'+item.thumbnail.extension}}
             >
              <Text style={{fontSize: 18, fontWeight:'bold', color:'white'}}>{item.name}</Text>
                <Text style={{color:'white'}}>
                  {item.description}
                </Text>
                <Text style={{color:'white'}}>
                  {this.state.copyright}
                </Text>
                <Text style={{marginBottom: 10, color:'white'}}>
                  {"Id: "+item.id}
                </Text>
                <Button
                  backgroundColor='#696969'
                  fontFamily='Lato'
                  onPress={() => navigate('View', {url_img:""+item.thumbnail.path+'.'+item.thumbnail.extension,name:item.name})}
                  buttonStyle={{borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 0}}
                  title='VER DETALLES'/>
            </Card>
          }
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  outerContainer: {
    position: 'relative',
    backgroundColor: 'rgb(52, 73, 94)',
    borderBottomColor: 'rgb(52, 73, 94)',
    borderBottomWidth: 1,
    padding: 12,
    height: 53,
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
