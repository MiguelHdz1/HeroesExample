import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {StackNavigator} from 'react-navigation';
import home from './components/home';
import view from './components/view';

const Application=StackNavigator({
  Home:{screen: home},
  View:{screen:view},
  },{
    navigationOptions:{
      header:false,
    }
  }
  );

export default class App extends Component{
  
  render() {
    return (
      <Application />
    );
  }
}
